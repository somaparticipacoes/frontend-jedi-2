![picture](https://media-exp1.licdn.com/dms/image/C4E0BAQEcVjHbJW50Pg/company-logo_200_200/0?e=2159024400&v=beta&t=_X9k75oEg4WyZUoNmhFkC103jBOH77vPK8i9TZnTxqI)

## Challenge - Frontend developer

O objetivo deste desafio é avaliar seu domínio do React Native: organização, estilo e boas práticas com código, criação dos componentes, conhecimento nas tecnologias e estruturas.

Você será avaliado pela sua capacidade de escrever um código simples, de fácil manutenção, e pela quantidade de funcionalidades que você entregar.

### Instruções

- **Tecnologias:** React Native
- **Objetivo do Projeto:** Criar uma aplicação mobile para listagem, pesquisa e visualização de dados de filmes.
- **User Interface:** O layout a ser seguido está [neste link](https://www.figma.com/file/jPvmadfMrTpYCJGsXKOsOI/Filme-teste-soma?node-id=1%3A70) como todo o DSM (Design System Manager).
- **Entregáveis:** Um repositório em um git público (github, gitlab, etc.)

---

## **Desafio**

> ### **Funcionalidades Básicas**

- Home screen
	- Listagem de 10 filmes em formato de banner horizontal;
	- Uma tab navigation (Não precisar ser funcional)
	- Um filtro com estilização de "tag selection" (A filtragem dos intens não precisa ser funcional)
- Search screen
	- O init da screen precisa trazer uma lista de buscas recentes (Os itens da busca podem ser fakes)
	- O input de busca deve ser funcional
- Movie Detail screen
	- A screen deve ter a capa do filme, avaliação, titulo, data de lançamento, duração sinopse e lista de atores com scrool horizontal
	- O usuario poderá avaliar o filme clicando no componente "Avalie". Essa ação abrirá um zz com um "rating stars"

> ### **Funcionalidades diferenciais não obrigatórias**

- Alteração entre modo dark e ligth;
- Filtros e navegação via tab navigation na home screen
- Buscas recentes em local storage

---

### O que nós vamos avaliar

- Você será avaliado pela qualidade do código, legibilidade e montagem das telas e componentes;
- Você é livre para tomar as decisões técnicas com as quais você se sente mais confortável;

### Dicas

- Dê prioridade a utilização de hooks;
- Caso precise utilizar estados globais aconcelhamos utilizar a biblioteca [zustend](https://github.com/pmndrs/zustand), mas sinta-se a vontade para utilzar o que te deixa mais confortável;
- A organização das branches e os commits no repositório falam muito sobre como você organiza seu trabalho.
- Você pode utilizar bibliotecas de componentes visuais;
- O material de UI/UX que fornecemos deve servir como uma referência de arquitetura, navegação e criação dos componentes;
- Use boas práticas de programação.

---

> ### **API**

**[Movies DB](https://developers.themoviedb.org/3/getting-started/introduction)**

- [Listar os filmes](https://developers.themoviedb.org/4/list/get-list)
- [Detalhes de um filme](https://developers.themoviedb.org/3/movies/get-movie-details)

**Obs.: Fique a vontade para utilizar outra API se preferir**

---

## FAQ

#### Posso utilizar frameworks/bibliotecas?

Sim.

> **Ex.:** Styled-componentes, React Native Paper e etc..

#### React, Flutter, Angular ou Vue?

Você precisa implementar em React Native.

#### Quanto tempo eu tenho ?

O tempo que você precisar, mas acreditamos que 5 dias são suficientes, caso haja algum impeditivo fale com agente estamos abertos a te escutar.

## Boa Sorte!

Sinta-se à vontade para fazer qualquer pergunta durante o desenvolvimento.
